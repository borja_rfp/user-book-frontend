import React from 'react';
import ReactDOM from 'react-dom';
import UserList from './components/userList/userList';
import UserForm from './components/userForm/userForm';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

const client = new ApolloClient({
    uri: 'http://localhost:3000/graphql',
});

const App = () => (
    <ApolloProvider client={client}>
        <UserList />
        <UserForm />
    </ApolloProvider>
);
ReactDOM.render(<App />, document.getElementById('root'));
