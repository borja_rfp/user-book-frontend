import React, { useState, useCallback} from 'react';
import { gql } from "apollo-boost";
import { useQuery } from '@apollo/react-hooks';

const GET_USERS = gql`
{
    users {
      id,
      name,
      first_name,
      email,
      address {
        address,
        zp,
        city,
        country
      }
      phone {
        phone
      }
    }
  }
`;

const UserList = () => {
    const { loading, error, data } = useQuery(GET_USERS);

    window.addEventListener('forceUpdate', () => {
        console.log('quique');
    });

    if (loading) return <p>Loading...</p>;
    if(error) return <p>Something went wrong :(</p>;

    return data.users.map((user) => (
        <div key={user.id}>
            <p>Name: {user.name}</p>
        </div>
    ));
};

export default UserList;
