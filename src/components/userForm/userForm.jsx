import React from 'react';
import { gql } from "apollo-boost";
import { useForm } from 'react-hook-form';
import { useMutation } from '@apollo/react-hooks';

const ADD_USER = gql`
  mutation CreateUser($createUserInput: CreateUserInput!, $createAddressInput: CreateAddressInput!, $createPhoneInput: CreatePhoneInput!){
  createUser(createUserInput: $createUserInput, createAddressInput: $createAddressInput, createPhoneInput: $createPhoneInput ) {
    id
  }
}
`;
const UserForm = () => {
    const { register, handleSubmit, reset } = useForm();
    const [createUser] = useMutation(ADD_USER);
    const onSubmit = async (formData) => {
        await createUser({
            variables: {
                createUserInput: {
                    name: formData.name,
                    first_name: formData.first_name,
                    email: formData.email,
                },
                createAddressInput: {
                    address: formData.address,
                    zp: formData.zp,
                    city: formData.city,
                    country: formData.country,
                },
                createPhoneInput: {
                    phone: formData.phone,
                    country_code: formData.phone_country_code,
                }

            }
        });
        // reset();
        console.log('antesd del dispatch event');
        window.dispatchEvent(new Event('forceUpdate'));
    };
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div>
                <label>Name:</label>
                <input type="text" name="name" ref={register} />
            </div>
            <div>
                <label>First name:</label>
                <input type="text" name="first_name" ref={register} />
            </div>
            <div>
                <label>Email:</label>
                <input type="text" name="email" ref={register} />
            </div>
            <div>
                <label>Address:</label>
                <input type="text" name="address" ref={register} />
            </div>
            <div>
                <label>Zip code:</label>
                <input type="text" name="zp" ref={register} />
            </div>
            <div>
                <label>City:</label>
                <input type="text" name="city" ref={register} />
            </div>
            <div>
                <label>Country:</label>
                <input type="text" name="country" ref={register} />
            </div>
            <div>
                <label>Phone:</label>
                <input type="text" name="phone" ref={register} />
            </div>
            <div>
                <label>Phone country code:</label>
                <input type="text" name="phone_country_code" ref={register} />
            </div>
            <div>
            <button type="submit">Add User</button>
            </div>
        </form>
    );
};

export default UserForm;
